package dtu.cairo;

import dtu.ws.fastmoney.*;
import java.math.BigDecimal;
/**
 * @author Mathias Kirkeskov Madsen
 * Implementation of bankcontroller
 */
public class BankConnector implements IBankController {

    BankService bs;
    Account merchantAcc;
    Account customerAcc;


    public BankConnector() {
        bs = new BankServiceService().getBankServicePort();
    }

    /**
     * @author Mathias Kirkeskov Madsen 
     * @param BigDecimal amount
     * @param String creditor
     * @param String description
     * @param String debtor
     * @throws BankServiceException_Exception
     * @return nothing
     */
    public void TransferMoney(BigDecimal amount, String creditor, String debtor, String description) throws BankServiceException_Exception {
        bs.transferMoneyFromTo(debtor, creditor, amount, description);
    }

    /**
     * @author Mathias Kirkeskov Madsen
     * @param BigDecimal amount
     * @param String creditorCpr
     * @param String customerCpr
     * @param String description
     * @return nothing
     */
    public void TransferMoneyCpr(BigDecimal amount, String creditorCpr, String customerCpr, String description) throws Exception {
        System.out.println("Tryng to transfer from " + customerCpr + " to " + creditorCpr);
        merchantAcc = bs.getAccountByCprNumber(creditorCpr);
        customerAcc = bs.getAccountByCprNumber(customerCpr);

        bs.transferMoneyFromTo(customerAcc.getId(), merchantAcc.getId(), amount, description);
    }
}
