package dtu.cairo;

import java.math.BigDecimal;
/**
 * @author Mahtias Kirkeskov Madsen
 * Interface for the bank controller
 */
public interface IBankController {
    /**
     * @author Mathias Kirkeskov Madsen
     * @param BigDecimal amount
     * @param String creditor
     * @param String debtor
     * @param String description
     * @return void
     */
    void TransferMoneyCpr(BigDecimal amount, String creditor, String debtor, String description) throws Exception;
}
