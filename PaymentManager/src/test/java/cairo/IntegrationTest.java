package cairo;


import dtu.ws.fastmoney.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import javax.ws.rs.core.Response;

import dtu.cairo.BankConnector;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * @author Mathias Kirkeskov Madsen
 * Tests for testing whether the interaction with the bank is as expected.
 * Will fail if it is not possible to connect to the bank. This is desired,
 * as if we can't talk with the bank we can't deploy. 
 */
public class IntegrationTest {
    BankConnector bc;

    User merchant;
    User customer;
    Account customerAcc;
    Account merchantAcc;
    String tokenId;
    String tokenURL;
    Response paymentResponse;
    String test = "";
    BankService bs;

    @Before
    public void SetBankConnector() throws Exception {
        bs = new BankServiceService().getBankServicePort();
        bc = new BankConnector();
        createAccount();
        createMerchant();

    }

    @Test
    public void testSuccessfulPayment() throws Exception {
        BigDecimal amount = new BigDecimal(50);
        bc.TransferMoney(amount, merchantAcc.getId(), customerAcc.getId(), "non-empty");
        Thread.sleep(100);
        assertEquals(new BigDecimal(150), bs.getAccount(customerAcc.getId()).getBalance());
        assertEquals(new BigDecimal(250), bs.getAccount(merchantAcc.getId()).getBalance());
    }

    @Test
    public void testSuccessfulPaymentCpr() throws Exception {
        BigDecimal amount = new BigDecimal(50);
        Thread.sleep(100);
        bc.TransferMoneyCpr(amount, merchantAcc.getUser().getCprNumber(), customerAcc.getUser().getCprNumber(), "non-empty");
        assertEquals(new BigDecimal(150), bs.getAccount(customerAcc.getId()).getBalance());
        assertEquals(new BigDecimal(250), bs.getAccount(merchantAcc.getId()).getBalance());
    }

    @Test (expected = BankServiceException_Exception.class)
    public void testNotEnoughMoney() throws Exception {
        BigDecimal amount = new BigDecimal(201);
        bc.TransferMoneyCpr(amount, merchantAcc.getUser().getCprNumber(), customerAcc.getUser().getCprNumber(), "non-empty");
    }

    @Test (expected = BankServiceException_Exception.class)
    public void testInvalidCpr() throws Exception {
        BigDecimal amount = new BigDecimal(1);
        bc.TransferMoneyCpr(amount, "5289113423", customerAcc.getUser().getCprNumber(), "non-empty");
    }

    @Test (expected = BankServiceException_Exception.class)
    public void testNegativeAmount() throws Exception {
        BigDecimal amount = new BigDecimal(-1);
        bc.TransferMoneyCpr(amount, merchantAcc.getUser().getCprNumber(), customerAcc.getUser().getCprNumber(), "non-empty");
    }

    @Test
    public void testTransferNothing() throws Exception {
        BigDecimal amount = new BigDecimal(0);
        bc.TransferMoneyCpr(amount, merchantAcc.getUser().getCprNumber(), customerAcc.getUser().getCprNumber(), "non-empty");
        Thread.sleep(100);
        assertEquals(new BigDecimal(200), bs.getAccount(customerAcc.getId()).getBalance());
        assertEquals(new BigDecimal(200), bs.getAccount(merchantAcc.getId()).getBalance());
    }


    /**
     * @author Mathias Kirkeskov Madsen
     * Helper function to create merchant
     * @throws Exception
     */
    private void createMerchant() throws Exception {
        BigDecimal startingAmount = new BigDecimal(200);
        merchant = new User();
        String cpr = "9999999999";
        merchant.setCprNumber(cpr);
        merchant.setFirstName("John");
        merchant.setLastName("Smokey");
        String acc = "";
        try{
            merchantAcc = bs.getAccountByCprNumber(cpr);
            bs.retireAccount(merchantAcc.getId());
        }catch(Exception e){		}
        try {
            acc = bs.createAccountWithBalance(merchant, startingAmount);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }

        merchantAcc = bs.getAccount(acc);
        assertEquals(startingAmount,merchantAcc.getBalance());
    }

    /**
     * @author Mathias Kirkeskov Madsen
     * Helper function to create user account
     * @throws Exception
     */
    private void createAccount() throws Exception{
        BigDecimal startingAmount = new BigDecimal(200);
        customer = new User();
        String cpr = "1234567890";
        customer.setCprNumber(cpr);
        customer.setFirstName("Jane");
        customer.setLastName("Doe");
        String acc = "";
        try{
            customerAcc = bs.getAccountByCprNumber(cpr);
            bs.retireAccount(customerAcc.getId());
        }catch(Exception e){		}
        try {
            acc = bs.createAccountWithBalance(customer, startingAmount);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }

        customerAcc = bs.getAccount(acc);
        assertEquals(startingAmount,customerAcc.getBalance());
    }

    @After
    public void teardown() throws BankServiceException_Exception {
        bs.retireAccount(customerAcc.getId());
        bs.retireAccount(merchantAcc.getId());
    }

}
