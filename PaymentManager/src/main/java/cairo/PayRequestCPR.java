package cairo;

import java.math.BigDecimal;

/**
 * @author Jonas
 * Used as object to parse to User-serive to validate customer and merchant
 * When recived from user-service it is considered as a valid pay request
 */

public class PayRequestCPR implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
    public PayRequestCPR(){

    }

    public String merchantId;

    public String customerId;

    public BigDecimal amount;
}
