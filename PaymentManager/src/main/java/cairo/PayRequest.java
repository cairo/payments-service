package cairo;

import java.math.BigDecimal;

/**
 * @author Jonas
 * An object to pass in message queues
 * Comes from a rest pay request
 * Being sent to Tokenmanager to get relevant user in the transaction and validate token
 */

public class PayRequest implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	public PayRequest(){

    }

    public String merchantId;

    public String tokenId;

    public BigDecimal amount;
}
