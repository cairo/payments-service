package cairo;

import java.io.IOException;

import javax.ws.rs.core.Response;

/**
 * @author Mathias Kirkeskov Madsen
 */
public interface IPaymentController {
    void PayMoneyWithToken(PayRequest pr);
    Response PayMoneyWithCpr(PayRequestCPR pr);
    void ValidateUsers(PayRequestCPR pr) throws IOException;

}
