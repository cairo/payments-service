package cairo;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.commons.lang3.SerializationUtils;
import java.io.IOException;

/**
 * @author Jonas
 * Produces messages for payment service into MQ
 */

public class ProducerMQ{
    private static String QUEUE_NAME_TM = "PsTm";
    private static String QUEUE_NAME_US = "PsUs";

    /**
     * @param pr : PayRequest object.
     * Produces a PayRequest into channel PsTm to start a validate token
     * @throws IOException
     */

    public void producePayRequest(PayRequest pr) throws IOException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("02267-cairo.compute.dtu.dk");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME_TM, false, false, false, null);

        byte[] data = SerializationUtils.serialize(pr);

        channel.basicPublish("", QUEUE_NAME_TM, null, data);
        System.out.println("payment service producer sent message,"
        		+ " tokenId: " + pr.tokenId + ", merchId: " + pr.merchantId + ", Amount: " + pr.amount);
        channel.close();
        connection.close();
    }

    /**
     * Sends a message over the channel PsUs to validate that both user and merchant is registered in DTUpay
     * @param pr : PayRequest object
     * @throws IOException
     */

    public void produceUserValidation(PayRequestCPR pr) throws IOException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("02267-cairo.compute.dtu.dk");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME_US, false, false, false, null);
        
        byte[] data = SerializationUtils.serialize(pr);
        System.out.println("payment service producer requests validation of users");
        System.out.println("Data: CustomerId: " + pr.customerId + ", MerchId: " + pr.merchantId + ", Amount: " + pr.amount);

        channel.basicPublish("", QUEUE_NAME_US, null, data);
        
        channel.close();
        connection.close();
    }
}