package cairo;

import dtu.cairo.BankConnector;
import dtu.cairo.IBankController;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * @author Mathias Kirkeskov Madsen
 * Business logic for handling payments.
 */
public class PaymentController implements IPaymentController {
    ProducerMQ mqp;
    ConsumerMQ mqc;

    /**
     * @author Mathias Kirkeskov Madsen
     * Initializes payment controller
     * @throws IOException
     */
    public PaymentController() throws IOException {
        this.mqp = new ProducerMQ();
        this.mqc = new ConsumerMQ(this);
    }

    /**
     * @author Mathias Kirkeskov Madsen
     * Transfers mone to the bank based on merchant and customer cpr.
     * @param IBankController bc
     * @param BigDecimal amount
     * @param String merchantId The cpr number of the merchant
     * @param String cpr The cpr number of the customer
     * @return Response Whether the payment was succesful
     */
    public Response TransferMoneyToBankCpr(IBankController bc, BigDecimal amount, String merchantId, String cpr) {
        System.out.println("Trying to transfer money to cpr=" + cpr);
        try {
            bc.TransferMoneyCpr(amount, merchantId, cpr, "transaction");
            System.out.println("\tSucces?");
            return Response.ok("Payment successful").build();
        }catch(Exception e){
            System.out.println("\tFailed");
            return Response.ok("Payment failed").build();
        }
    }

    /**
     * @author Mathias Kirkeskov Madsen
     * Pay money using a token
     * @param PayRequest pr
     */
    public void PayMoneyWithToken(PayRequest pr) {
        try {
            UseToken(pr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @author Mathias Kirkeskov Madsen
     */
    public Response PayMoneyWithCpr(PayRequestCPR pr) {
        return TransferMoneyToBankCpr(new BankConnector(), pr.amount, pr.merchantId, pr.customerId);
    }

    /**
     * @author Mathias Kirkeskov Madsen
     * Ask the token service to use the supplied token
     * @param pr
     * @throws IOException
     */
    private void UseToken(PayRequest pr) throws IOException {
        mqp.producePayRequest(pr);
    }
    
    /**
     * @author Mathias Kirkeskov Madsen
     * Ask the token service to validate the cpr 
     * @throws IOException
     */
    public void ValidateUsers(PayRequestCPR pr) throws IOException {
    	mqp.produceUserValidation(pr);
    }
}
