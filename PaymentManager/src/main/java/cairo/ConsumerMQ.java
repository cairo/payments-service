package cairo;

import java.io.IOException;

import com.rabbitmq.client.*;
import org.apache.commons.lang3.SerializationUtils;

public class ConsumerMQ {

    /**
     * @author Daniel
     * Consumer for grabbing messages from the message queues and either validate or pay depending on which queue.
     */
    private static String QUEUE_NAME_TM = "TmPs";
    private static String QUEUE_NAME_US = "UsPs";
    private IPaymentController paymentController;

    /**
     * @author Daniel
     * @param PayConInterface IPaymentController
     * @throws IOException Exception
     */
    public ConsumerMQ(IPaymentController PayConInterface) throws IOException {
        this.paymentController = PayConInterface;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("02267-cairo.compute.dtu.dk");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME_TM, false, false, false, null);
        channel.queueDeclare(QUEUE_NAME_US, false, false, false, null);

        Consumer consumerTM = new DefaultConsumer(channel) {
            /**
             * @author Daniel
             * This handledelivery is connected to consumerTM
             * @param consumerTag String
             * @param envelope Envelope
             * @param properties AMQP.BasicProperties
             * @param body byte[]
             */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                PayRequestCPR pr = SerializationUtils.deserialize(body);
                System.out.println("payment service consumer got message");
                System.out.println("Data: CustomerId: " + pr.customerId + ", MerchId: " + pr.merchantId + ", Amount: " + pr.amount);
                try {
					paymentController.ValidateUsers(pr);
				} catch (IOException e) {
					System.out.println("Exception in consumer from Token");
					e.printStackTrace();
				}
                //paymentController.PayMoneyWithCpr(pr);
            }
        };
        
        Consumer consumerUS = new DefaultConsumer(channel) {
            /**
             * @author Daniel
             * This handledelivery grabs messages as the consumer: consumerUS
             * @param consumerTag
             * @param envelope
             * @param properties
             * @param body
             */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
            	PayRequestCPR pr = SerializationUtils.deserialize(body); 
            	
            	System.out.println("Payment service got Users Validated");
                System.out.println("Data: CustomerId: " + pr.customerId + ", MerchId: " + pr.merchantId + ", Amount: " + pr.amount);
            	
            	paymentController.PayMoneyWithCpr(pr);
            }
        };

        channel.basicConsume(QUEUE_NAME_TM, true, consumerTM);
        channel.basicConsume(QUEUE_NAME_US, true, consumerUS);
    }
}
