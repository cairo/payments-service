package cairo;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * @author Henning
 * This class defines which methods that can be called
 * through the rest adapter associated with the payment microservice.
 */
@Path("/payment")
public class paymentEndpoint {

    /**
     * This function allows a client to make a HTTP get request to this rest adapter.
     * It is only used for pinging purposes, to check when the microservice is running.
     * @return Response ok
     */
    @GET
    @Produces("text/plain")
    public Response doGet(){
        return Response.ok("Your resource is working!").build();
    }

    /**
     * This function is an entry point to the payment controller, which
     * will send a message to the tokenManager micro service and consume a token.
     *
     * @param PayRequest pr
     * @return Response ok
     */
    @POST
    @Consumes("application/json")
    @Produces("text/plain")
    public Response payMoneyWithToken(PayRequest pr){
        RestApplication.pc.PayMoneyWithToken(pr);
        return Response.ok("Payment successful").build();
    }

}
