package cairo;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.io.IOException;


/**
 * @author Henning
 * This class is the entry point for the rest adapter.
 */
@ApplicationPath("/")
public class RestApplication extends Application {
    public static IPaymentController pc;
    public RestApplication(){
        super();
        try{
            pc = new PaymentController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
